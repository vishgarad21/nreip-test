import org.springframework.web.client.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import java.util.*;
import org.json.simple.JSONObject;

final String uri = "http://192.168.77.205:8081/api/v1/auth/cms/login";

// create request body
JSONObject request = new JSONObject();
request.put('email', 'CmsUser@Xoriant.com');
request.put('password', 'A#$A120&Asd');

HttpHeaders headers = new HttpHeaders();
headers.setContentType(MediaType.APPLICATION_JSON);
HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

RestTemplate restTemplate = new RestTemplate();
Object answer = restTemplate.postForObject(uri, entity, Object.class);
return answer ;