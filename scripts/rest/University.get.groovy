import org.springframework.web.client.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import java.util.*;
import org.json.simple.JSONObject;

//import org.apache.http.HttpHeaders;

//def result=[:]
//http://cms.mocklab.io/applicant
//https://reqres.in/api/users/2
//result.products=[]
//final String uri = "http://192.168.77.205:8083/api/v1/program/auth/verify/otp";
final String uri = "http://192.168.77.205:8084/api/v1/university";

// create request body
//JSONObject request = new JSONObject();
//request.put("email", "vishal.garad@xoriant.com");
//request.put("otp", "123456");
//request.put('email', 'CmsUser@Xoriant.com');
//request.put('password', 'A#$A120&Asd');

HttpHeaders headers = new HttpHeaders();
headers.setContentType(MediaType.APPLICATION_JSON);
//HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

RestTemplate restTemplate = new RestTemplate();
Object[] answer = restTemplate.getForObject(uri, Object[].class);
//Object[] result1 = restTemplate.getForObject(uri,Object[].class);
return  answer;