<#import "/templates/system/common/cstudio-support.ftl" as studio />
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <style>
        * {
            top: 0;
            bottom: 0;
            box-sizing: border-box;
        }

        body {
            font-family: "Open Sans", sans-serif;
            overflow-x: hidden;
        }


        .footer{
            bottom: 0;
        }
        
        .footer_items {
            padding: 20px 45px;
        }
        
        .footer_items ul {
            margin:15px 40px;
        }
        
        .footer_items ul li {
            padding-right: 20px;
            font-size: 14px;
            text-transform: capitalize;
            line-height: 4em;
        }
        .footer_items ul li a {
            color: #C0BAB6;
        }
        .footer_items ul li a:hover {
            color: #80def5;
        }
        
        .footer-content .font {
            font-size: 12px;
            color: #C0BAB6;

        }
        .footer-content{
            padding: 10px 20px;
        }

        .font a {
            text-decoration: none;
        }

        
        @media (max-width: 576px) {
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }
        }
    </style>
</head>

<body>

    <div class="bg-dark text-white footer">
       <div class="container">
        <div class="footer_items">
            <div class="row mt-2">
                <div class="col-md-12 col-sm-12">
                    <ul class="list-inline font" <@studio.iceAttr />>
                        <li class="list-inline-item"><a href="/">${contentModel.footerlistitem1_s}</a></li>
                        <li class="list-inline-item"><a href="/details">${contentModel.footerlistitem2_s}</a></li>
                        <li class="list-inline-item"><a href="/lab_contacts">${contentModel.footerlistitem3_s}</a></li>
                        <li class="list-inline-item"><a href="/labs">${contentModel.footerlistitem4_s}</a></li>
                        <li class="list-inline-item"><a href="/eligibility">${contentModel.footerlistitem5_s}</a></li>
                        <li class="list-inline-item"><a href="/faqs">${contentModel.footerlistitem6_s}</a></li>
                        <li class="list-inline-item"><a href="/events">${contentModel.footerlistitem7_s}</a></li>
                        <li class="list-inline-item"><a href="/promotionalposter">${contentModel.footerlistitem8_s}</a></li>
                        <li class="list-inline-item"><a href="/internquardcharts">${contentModel.footerlistitem9_s}</a></li>
                        <li class="list-inline-item"><a href="/covidlabstatus">${contentModel.footerlistitem10_s}</a></li>
                        <li class="list-inline-item"><a href="/apply">${contentModel.footerlistitem11_s}</a></li>
                    </ul>

                </div>
            </div>
        </div>
     </div>
        <hr />
        <div class="container">
            <div class="footer-content">
                <div class="row gx-5 font">
                    <div class="col-md-8 col-sm-12">
                        <span <@studio.iceAttr />>${contentModel.companyandaddress_html}</span>
                        <div class="mt-3" <@studio.iceAttr />>${contentModel.applyonlinelink_s}</div>
                        <div class="mt-3" <@studio.iceAttr />>${contentModel.copyright_html}</div>
                        <div class="mt-3" <@studio.iceAttr />>${contentModel.template_html}</div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="float-end" <@studio.iceAttr />>
                            <span>Administered by</span> <img src="${contentModel.administeredlogo_s}" height="70" width="80" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <@studio.toolSupport />
</body>

</html>