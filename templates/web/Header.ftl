<#import "/templates/system/common/cstudio-support.ftl" as studio />
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    
        
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .font {
            font-size: 14px;
        }

        .footer-content .font {
            font-size: 12px;
            margin: 15px 40px;
        }

        .font a {
            text-decoration: none;
        }

        .header {
            margin: 0 60px;
        }

        .list_items ul {
            margin: 0 60px;
        }

        .list_items ul li {
            padding-right: 20px;
            padding-bottom: 20px;
            text-transform: uppercase;
        }
        .list_items ul li a {
            color: #C0BAB6;
        }
        .list_items ul li a:hover {
            color: #80def5;
        }

        @media (max-width: 576px) {
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }
        }
    </style>
</head>

<body>

    <div class="bg-dark text-light">
        <div class="container">
            <div class="header py-3">
                <div class="row">
                    <div class="col-md-7 ">
                        <a href="/"><img src="${contentModel.logo_s}" height="120" width="400" <@studio.iceAttr /> /></a>
                    </div>
                    <div class="col-md-5 pt-3">
                        <div class="float-end">
                            <span class="text-info" <@studio.iceAttr />><i class="fas fa-envelope me-2"></i>${contentModel.email_s}</span> <span
                                class="px-2">&#124;</span>
                            <span <@studio.iceAttr />><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-telephone-fill me-2" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                                </svg>
                                ${contentModel.number_s}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="container">
            <div class="row list_items">
                <div class="col-md-12 col-sm-12">
                    <ul class="list-inline font text-light" <@studio.iceAttr />>
                        <li class="list-inline-item"><a href="/">${contentModel.headerlistitem1_s}</a></li>
                        <li class="list-inline-item"><a href="/details">${contentModel.headerlistitem2_s}</a></li>
                        <li class="list-inline-item"><a href="/lab_contacts">${contentModel.headerlistitem3_s}</a></li>
                        <li class="list-inline-item"><a href="/labs">${contentModel.headerlistitem4_s}</a></li>
                        <li class="list-inline-item"><a href="/eligibility">${contentModel.headerlistitem5_s}</a></li>
                        <li class="list-inline-item"><a href="/faqs">${contentModel.headerlistitem6_s}</a></li>
                        <li class="list-inline-item"><a href="/events">${contentModel.headerlistitem7_s}</a></li>
                        <li class="list-inline-item"><a href="/promotionalposter">${contentModel.headerlistitem8_s}</a></li>
                        <li class="list-inline-item"><a href="/internquardcharts">${contentModel.headerlistitem9_s}</a></li>
                        <li class="list-inline-item"><a href="/covidlabstatus">${contentModel.headerlistitem10_s}</a></li>
                        <li class="list-inline-item"><a href="/apply">${contentModel.headerlistitem11_s}</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
<@studio.toolSupport />
</body>

</html>