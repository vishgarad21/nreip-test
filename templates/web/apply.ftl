<#import "/templates/system/common/cstudio-support.ftl" as studio />
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <style>
        

        * {
            box-sizing: border-box;
            top: 0;
            bottom: 0;
        }
        
        body {
            font-family: "Open Sans", sans-serif;
        }
        
        .footer{
            bottom: 0;
        }
        .footer-content .font {
            font-size: 14px;
            font-style: italic;
            color:#A9A9A9;
        }

        .font a {
            text-decoration: none;
        }
        .footer-content{
            padding-top: 30px;
        }
        .header {
            margin: 0 60px;
        }

        .apply {
            margin: 50px 0;
        }
        .errormsgboxsucess{
            background-color:#5ae891;
            height: 30px;
            width:90%;
            margin-left: 60px;
            border: 1px solid #5ae891;
        }
       .errormsgboxfailure{
            background-color:#f54e4e;
            height: 30px;
            width:90%;
            margin-left: 60px;
            border: 1px solid #f54e4e;
        }
        .show{
            display:block;
        }
        .hide{
            display:none;
        }
    </style>
</head>

<body>
<div class="bg-dark text-light">
        <div class="container">
            <div class="header py-3">
                <div class="row">
                    <div class="col-md-7 ">
                        <a href="/"><img src="${contentModel.applylogo_s}" height="120" width="400" <@studio.iceAttr /> /></a>
                    </div>
                    <div class="col-md-5 pt-3">
                        <div class="float-end">
                            <span class="text-info" <@studio.iceAttr />><i class="fas fa-envelope me-2"></i>${contentModel.applyemail_s}</span> <span
                                class="px-2">&#124;</span>
                            <span <@studio.iceAttr />><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-telephone-fill me-2" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                                </svg>
                                ${contentModel.applymobilenumber_s}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
                
    <section class="apply">
                <div  >
                <p id ="errorMsg" class="hide"></p>
                </div>
        <div class="container">
                
            <div class="row gx-5">
                <div class="col-md-6 col-sm-12">
                   
                        <div class="text-secondary mb-4">
                            <h3 <@studio.iceAttr /> class="font"> <a href="${contentModel.labsURL_s}"  target="_blank">${contentModel.applylogin_s}</a></h3>
                        </div>
                        
                   
                </div>
                <div class="col-md-6 col-sm-12">
                
                    <form id="notify-form" method="post" runat="server">
                        <div class="text-secondary mb-4" <@studio.iceAttr />>
                            <h3>${contentModel.applysignup_s}</h3>
                        </div>
                        <div class="mb-3" <@studio.iceAttr />>
                            ${contentModel.applysignuptext1_html}
                        </div>
                        <div class="mb-3" <@studio.iceAttr />>
                            ${contentModel.applysignuptext2_html}
                        </div>
                        <div class="mb-3">
                             <label for="exampleInputFirstName" class="form-label" <@studio.iceAttr />>First Name</label>
                            <input type="text" class="form-control" id="firstName"
                                aria-describedby="emailHelp">
                          <label for="exampleInputLastName" class="form-label" <@studio.iceAttr />>Last Name</label>
                            <input type="text" class="form-control" id="lastName"
                                aria-describedby="emailHelp">
                            <label for="exampleInputEmail1" class="form-label" <@studio.iceAttr />>${contentModel.applysignupemailinput_s}</label>
                            <input type="email" class="form-control" id="email"
                                aria-describedby="emailHelp">
                        </div>
                        <input type="button" class="btn btn-outline-secondary ms-2" onclick=" requestForProgramStart()"  <@studio.iceAttr /> value="${contentModel.applysignupnotificationbutton_s}"/>
                    </form>
                </div>
            </div>


        </div>
    </section>
    <div class="bg-dark text-white footer">
        <div class="container">
            <div class="footer-content">
                <div class="row font">
                    <div class="col-md-8 col-sm-12">
                        <span <@studio.iceAttr />>${contentModel.footercontent_html}</span>
                        
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="float-end" <@studio.iceAttr />>
                            <img src="${contentModel.footerimage_s}" height="50" width="100" />
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="py-2">
            <hr/>
        </div>
    </div>
    <@studio.toolSupport />
    <script>
function requestForProgramStart(){
            var firstNameData= document.getElementById("firstName").value;
            var lastNameData= document.getElementById("lastName").value;
            var emailToBeSent= document.getElementById("email").value;
            
          var dataToSend = {
                    firstName: firstNameData,
                    lastName: lastNameData,
                    email: emailToBeSent,
                    eventUserJson: null
                };
                 
               const response =  fetch('/api/EventUser.json', {
                      method: "POST",
                      body: JSON.stringify(dataToSend),
                      headers: {"Content-type": "application/json; charset=UTF-8","X-ProgramID": ${contentModel.programIDToCallApi_s}}
                    })
                    .then(response =>{
                       if(response.status==200){
                          var element = document.getElementById("errorMsg");
                           element.classList.remove("hide");
                            element.classList.remove("errormsgboxfailure");
                           document.getElementById("notify-form").reset();
                            element.innerHTML = "You will be notified when the application period opens!";
                            element.classList.add("show");
                            element.classList.add("errormsgboxsucess");
                       }
                       else{
                        var element = document.getElementById("errorMsg");
                           element.classList.remove("hide");
                           document.getElementById("notify-form").reset();
                            element.innerHTML = "There was a problem saving your notification request";
                            element.classList.add("show");
                            element.classList.add("errormsgboxfailure");
                       }
                      
                    return response.json()});
                    document.getElementById("notify-form").reset();
       
    }
    </script>

</body>

</html>