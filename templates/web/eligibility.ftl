<#import "/templates/system/common/cstudio-support.ftl" as studio />
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <style>
        body {
            font-family: "Open Sans", sans-serif;
        }

        * {
            box-sizing: border-box;
        }
        
        .eligibility h1{
            color: #80def5;
        }
        
        .eligibility {
            margin: 50px 0;
        }
        .eligibility h2{
            margin: 15px 0 30px 0;
            font-size: 1.8em;
            font-family: 'Roboto', sans-serif;
        }
        .eligibility p, .eligibility ul{
            margin-bottom: 30px;
            line-height: 2em;
        }

    </style>
</head>

<body>
<@renderComponent component=contentModel.header_o.item />
    <section class="eligibility">
        <div class="container">
            <div class="bg-dark px-4 py-2">
                <h1 <@studio.iceAttr />>${contentModel.eligibilityheader_s}</h1>
            </div>
            <div class="eligibility_data">
                <h2 <@studio.iceAttr />>${contentModel.eligibilitycriteria_s}</h2>
                <div <@studio.iceAttr />>${contentModel.eligibilitypoints_html}</div>
            </div>
        </div>
    </section>
<@renderComponent component=contentModel.footer_o.item />
    <@studio.toolSupport />

</body>

</html>