<#import "/templates/system/common/cstudio-support.ftl" as studio />
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <style>
        .bg-color {
            background-color: #e1f1f2;
        }

       .modal-backdrop.show {
            opacity: 0 !important;
        }


        .modal-dialog {
            max-width: 1200px !important;
        }
        
        .table_padding {
            padding: 30px 0 120px 0;
            
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 18px;
        }

        * {
            box-sizing: border-box;
        }

        .events {
            margin: 50px 0;
        }
    </style>
</head>

<body>

    <!---header section -->

   <@renderComponent component=contentModel.header_o.item />
   <section id="alert"></section>
        <section>
        <div class="container    table_padding">
            <table class="datatable table table-fixed table-hover align-middle border border-1 border-dark bg-white" id="data">
            
                <thead>
                    <tr>
                        <th scope="col">Event Name</th>
                        <th scope="col">Start Date</th>
                        <th scope="col">End Date</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody id="tbody">
                    
                </tbody>
                
            </table>
            <nav aria-label="Page navigation example" class="">
            <div class="row">
                <div class="col-md-9 d-flex justify-content-end">
                <div class="d-inline">
                    <div class="d-inline">
                        <span>Rows Per Page</span>
                    </div>
                    <div class="d-inline">
                    <select class="form-select-lg ms-2" style="width:70px"  id="rows_per_page" onchange="page_Size(this)">
                          <option selected value="5">5</option>
                          <option value="10">10</option>
                          <option value="25">25</option>
                    </select>
                    </div>
                </div>
                </div>
                <div class="col d-flex justify-content-end">
                    <ul class="pagination">
                        <li class="page-item" onclick="previous()"><a class="page-link me-3" href="#">Previous</a></li>
                        <li class="page-item" onclick="next()"><a class="page-link" href="#">Next</a></li>
                    </ul>
                </div>
                </div>
            </nav>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Register Now</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body mx-3">
                            <div class="row g-1 align-items-center">
                                <div class="col-auto">
                                    <input type="text" id="firstName" class="form-control w-75" placeholder="First Name"
                                        aria-describedby="firstName" required>
                                </div>
                                <div class="col-auto">
                                    <input type="text" id="lastName" class="form-control w-75" placeholder="Last Name"
                                        aria-describedby="passwordHelpInline" required>
                                </div>
                                <div class="col-auto">
                                    <input type="email" class="form-control w-75" id="email" placeholder="Email ID"
                                        aria-describedby="emailHelp" required>
                                </div>
                                <div class="col-auto">
                                    <select class="form-select w-100" aria-label="University" id="university_dropDown">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" id="submit" onclick=sendRegistrationData()>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id ="footer">
     <@renderComponent component=contentModel.footer_o.item />
    </section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
     <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
        crossorigin="anonymous"></script>
        
        <script>
        
        // mapping event data in table
        var apiForAuth=` ${contentModel.apiForAuthAdmin_s}`;
        var apiForEvents=`${contentModel.apiForEventsList_s}`;
        
        
          //var apiForAuth=" http://192.168.77.205:8081/api/v1/auth/cms/login";
        //var apiForEvents="http://192.168.77.205:8081/api/v1/admin/outreach/events/search";
        let programId = ${contentModel.programIDToCallApi_s};
        let page = 1;
        let pageSize = document.querySelector("#rows_per_page").value;
        let events_data;
     
        async function tableData() {
        <#noparse>
        const response = await fetch(`/api/ProgramEventList.json?page=${page}&size=${pageSize}&programiId=${programId}`,
        { method: 'GET',
        headers: {"Content-type": "application/json; charset=UTF-8","API-FOR-AUTH":apiForAuth,"API-FOR-EVENT":apiForEvents 
            }});
        </#noparse>
        const data = await response.json();
        events_data = data;
        var res = "";
        if(data.totalElements == 0){
            alert("No more events");
            return false;
        }else{
           data && data.elements.map((event)=>{
           var isoDateTime = new Date(event.startDate);
            var localDateTime = isoDateTime.toLocaleDateString() + " " + isoDateTime.toLocaleTimeString();
            var eventEndDate;
            if(event.endDate){
            var isoDateTime1 = new Date(event.endDate);
                eventEndDate = isoDateTime1.toLocaleDateString() + " " + isoDateTime1.toLocaleTimeString();
            }else{
                eventEndDate = "---------";
            }
            <#noparse>
                res += `
                        <tr>
                            <td> ${event.eventName} </td>
                            <td> ${localDateTime} </td>
                            <td> ${eventEndDate}</td>
                            <td><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="sendId(${event.id})">Register Now</button></td>
                        </tr>   `
                        
                return res;
                </#noparse>
            })
            }
            document.getElementById("tbody").innerHTML = res && res;
            
        }
        tableData();
        
        //pagination
        
        function page_Size(selected) {
          pageSize = selected.value;  
          tableData();
        }
        function previous(){
            if(page>1){
                page--;
                tableData();
            }else{
                return false;
            }
        }
    
        function next(){
        console.log(events_data.totalElements);
        if(events_data.totalElements == 0){
            return false;
        }
            page++;
            tableData();
        }
        
        // University dropdown data
        let universities;
        async function main() {
        
        const response = await fetch('/api/University.json');
        const data = await response.json();
        universities = data;
        var university = "";
            universities && universities.map((val) => {
                university += "<option>" + val.name + "</option>";
                return university;

            });
            document.getElementById("university_dropDown").innerHTML = university && university;
        }
        
        main();
        
    // post registration data to API
        
      const firstName = document.querySelector("#firstName");
      const lastName = document.querySelector("#lastName");
      const email = document.querySelector("#email");
      const university_name = document.querySelector("#university_dropDown");
      
      let event_id;
        function sendId(id) {
        var element = document.getElementById("exampleModal");
              element.style.visibility = "visible";
            event_id = id;
            console.log("event_id", event_id)
        }
        
     function sendRegistrationData(){
        let formValues = {};
            formValues.eventId = event_id;
            formValues.firstName = firstName.value;
            formValues.lastName = lastName.value;
            formValues.email = email.value;
            formValues.universityName = university_name.value;
            let university_id = universities.filter(val => val.name == formValues.universityName).map(val => val.id)
            formValues.universityId = university_id[0];
            
            fetch('/api/EventUser.json', {
            method: 'POST',
            body: JSON.stringify(formValues),
            headers: {"Content-type": "application/json; charset=UTF-8","X-ProgramID": ${contentModel.programIDToCallApi_s}}
            }).then( (response) => { 
                if (response.status === 200) {
                  let res = `<div class="alert alert-success" role="alert">
                                Registration is Successfull
                             </div>`
                            document.querySelector("#alert").innerHTML = res;
                            setTimeout(function(){
                                document.querySelector("#alert").innerHTML = '';
                            }, 3000);
                    } else {
                  let res = `<div class="alert alert-danger" role="alert">
                                Registration is Failed. Please Try Again
                             </div>`
                            document.querySelector("#alert").innerHTML = res;
                            setTimeout(function(){
                                document.querySelector("#alert").innerHTML = '';
                            }, 3000);
                }
            });
            
            var element = document.getElementById("exampleModal");
              element.style.visibility = "hidden";
              setTimeout(function(){
                                location.reload();;
                            }, 2000);
                
    }
    
    
    
     </script>
        
    <@studio.toolSupport />
</body>

</html>