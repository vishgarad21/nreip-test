<#import "/templates/system/common/cstudio-support.ftl" as studio />
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Bootstrap Static Navbar</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
        integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">


    <style>
        * {
            box-sizing: border-box;
        }

        .faqs h1{
            color: #80def5;
        }
        .faqsheader h2 {
            margin: 15px 0 30px 0;
            font-size: 1.8em;
            font-family: 'Roboto', sans-serif;
        }

        .faqs_data h3 {
            margin: 15px 0 30px 0;
            font-size: 1.4em;
            font-family: 'Roboto', sans-serif;
        }

        .faqs {
            margin: 50px 0;
        }

        .accordion-button {
            padding-left: 10px;
        }

        .accordion-flush .accordion-item {
            margin-bottom: 10px;
            border: 1px solid #e2e2e2 !important;
        }

        .accordion-item p {
            margin-top: 10px;
        }

        .accordion-flush h2 {
            margin-bottom: 0;
            margin-top: 0;
        }

        .accordion-flush .accordion-item .accordion-button::after {
            content: " \002B";
            position: absolute;
            left: 10px;
            padding: 5px;
            transform: scale(0.7) !important;
            color: black;
            text-align: center;
            display: flex;
            align-items: center;
            background-position: center;
            margin-top: -6px;
        }

        .accordion-flush .accordion-item .accordion-button:not(.collapsed)::after {
            content: " \2212";
            color: black;
            background-position: center;
            text-align: center;
            display: flex;
            align-items: center;

        }

        .accordion-button::after {
            width: 35px !important;
            height: 35px !important;
            font-size: 50px !important;
            background-image: none !important;

        }

        .accordion-button:not(.collapsed)::after {
            width: 35px !important;
            height: 35px !important;
            font-size: 50px !important;
            background-image: none !important;

        }

        .applicant {
            padding: 100px;
        }

        .accordion-flush .accordion-item .accordion-button:focus {
            box-shadow: none;
            border-color: rgba(0, 0, 0, .125);

        }

        .accordion-header button {
            color: #005699;
        }

        .accordion-header button:hover {
            color: black;
        }

        .accordion-flush .accordion-item .accordion-button {
            border-radius: 0;
            padding: 8px;
        }
        .accordion-flush{
            margin-bottom: 30px;
        }
    </style>
</head>

<body>
    <@renderComponent component=contentModel.header_o.item />
    <section class="faqs">
        <div class="container">
            <div class="bg-dark px-4 py-2">
                <h1 <@studio.iceAttr />>${contentModel.faqsheader_s}</h1>
            </div>
            <div class="faqs_data">
                <div class="faqsheader">
                    <h2 <@studio.iceAttr />>${contentModel.faqsheader1questions_s}</h2>
                </h2>
                <h3 <@studio.iceAttr />>${contentModel.faqsheader1information_s}</h3>
                <div class="accordion accordion-flush" id="accordionFlushExample" <@studio.iceAttr />>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseOne" aria-expanded="false"
                                aria-controls="flush-collapseOne">
                                <span class="ps-2">${contentModel.faqsquestionsaccordian1_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                ${contentModel.faqsquestionsaccordian1_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseTwo" aria-expanded="false"
                                aria-controls="flush-collapseTwo">
                                <span class="ps-2">${contentModel.faqsquestionsaccordian2_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                ${contentModel.faqsquestionsaccordian2_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree" aria-expanded="false"
                                aria-controls="flush-collapseThree">
                                <span class="ps-2">${contentModel.faqsquestionsaccordian3_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                ${contentModel.faqsquestionsaccordian3_data_html}
                            </div>
                        </div>
                    </div>
                </div>
                <h3 <@studio.iceAttr />>${contentModel.faqsheader3eligibility_s}</h3>
                <div class="accordion accordion-flush" id="accordionFlushExample1" <@studio.iceAttr />>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne1">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseOne1" aria-expanded="false"
                                aria-controls="flush-collapseOne1">
                                <span class="ps-2">${contentModel.faqseligibilityaccordian1_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseOne1" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingOne1" data-bs-parent="#accordionFlushExample1">
                            <div class="accordion-body">
                                ${contentModel.faqseligibilityaccordian1_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo2">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseTwo2" aria-expanded="false"
                                aria-controls="flush-collapseTwo2">
                                <span class="ps-2">${contentModel.faqseligibilityaccordian2_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseTwo2" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingTwo2" data-bs-parent="#accordionFlushExample1">
                            <div class="accordion-body">
                                ${contentModel.faqseligibilityaccordian2_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree3">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree3" aria-expanded="false"
                                aria-controls="flush-collapseThree3">
                                <span class="ps-2">${contentModel.faqseligibilityaccordian3_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree3" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree3" data-bs-parent="#accordionFlushExample1">
                            <div class="accordion-body">
                                ${contentModel.faqseligibilityaccordian3_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree4">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree4" aria-expanded="false"
                                aria-controls="flush-collapseThree4">
                                <span class="ps-2">${contentModel.faqseligibilityaccordian4_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree4" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree4" data-bs-parent="#accordionFlushExample1">
                            <div class="accordion-body">

                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree5">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree5" aria-expanded="false"
                                aria-controls="flush-collapseThree5">
                                <span class="ps-2">${contentModel.faqseligibilityaccordian5_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree5" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree5" data-bs-parent="#accordionFlushExample1">
                            <div class="accordion-body">

                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree6">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree6" aria-expanded="false"
                                aria-controls="flush-collapseThree6">
                                <span class="ps-2">${contentModel.faqseligibilityaccordian6_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree6" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree6" data-bs-parent="#accordionFlushExample1">
                            <div class="accordion-body">

                            </div>
                        </div>
                    </div>

                </div>

                <h3 <@studio.iceAttr />>${contentModel.faqsapplicationheader_s}</h3>
                <div class="accordion accordion-flush" id="accordionFlushExample2" <@studio.iceAttr />>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne22">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseOne22" aria-expanded="false"
                                aria-controls="flush-collapseOne22">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian1_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseOne22" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingOne22" data-bs-parent="#accordionFlushExample22">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian1_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo22">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseTwo22" aria-expanded="false"
                                aria-controls="flush-collapseTwo22">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian2_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseTwo22" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingTwo22" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian2_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree32">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree32" aria-expanded="false"
                                aria-controls="flush-collapseThree32">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian3_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree32" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree32" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian3_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree42">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree42" aria-expanded="false"
                                aria-controls="flush-collapseThree42">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian4_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree42" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree42" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian4_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree52">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree52" aria-expanded="false"
                                aria-controls="flush-collapseThree52">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian5_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree52" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree52" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian5_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree62">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree62" aria-expanded="false"
                                aria-controls="flush-collapseThree62">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian6_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree62" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree62" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian6_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree72">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree72" aria-expanded="false"
                                aria-controls="flush-collapseThree72">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian7_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree72" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree72" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian7_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree82">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree82" aria-expanded="false"
                                aria-controls="flush-collapseThree82">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian8_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree82" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree82" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian8_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree92">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree92" aria-expanded="false"
                                aria-controls="flush-collapseThree92">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian9_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree92" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree92" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian9_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree102">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree102" aria-expanded="false"
                                aria-controls="flush-collapseThree102">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian10_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree102" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree102" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian10_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree112">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree112" aria-expanded="false"
                                aria-controls="flush-collapseThree112">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian11_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree112" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree112" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian11_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree122">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree122" aria-expanded="false"
                                aria-controls="flush-collapseThree122">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian12_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree122" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree122" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian12_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree112">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree112" aria-expanded="false"
                                aria-controls="flush-collapseThree112">
                                <span class="ps-2">${contentModel.faqsapplicationaccordian13_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree112" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree112" data-bs-parent="#accordionFlushExample2">
                            <div class="accordion-body">
                                ${contentModel.faqsapplicationaccordian13_data_html}
                            </div>
                        </div>
                    </div>
                </div>



                <h3 <@studio.iceAttr />>${contentModel.fAQsTermsofAppointmentheader_s}</h3>
                <div class="accordion accordion-flush" id="accordionFlushExample3" <@studio.iceAttr />>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne3">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseOne3" aria-expanded="false"
                                aria-controls="flush-collapseOne3">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian1_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseOne3" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingOne3" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian1_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo3">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseTwo3" aria-expanded="false"
                                aria-controls="flush-collapseTwo3">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian2_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseTwo3" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingTwo3" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian2_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree33">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree33" aria-expanded="false"
                                aria-controls="flush-collapseThree33">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian3_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree33" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree33" data-bs-parent="#accordionFlushExample33">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian3_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree43">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree43" aria-expanded="false"
                                aria-controls="flush-collapseThree43">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian4_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree43" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree43" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian4_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree53">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree53" aria-expanded="false"
                                aria-controls="flush-collapseThree53">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian5_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree53" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree53" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian5_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree63">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree63" aria-expanded="false"
                                aria-controls="flush-collapseThree63">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian6_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree63" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree63" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian6_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree73">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree73" aria-expanded="false"
                                aria-controls="flush-collapseThree73">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian7_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree73" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree73" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian7_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree83">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree83" aria-expanded="false"
                                aria-controls="flush-collapseThree83">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian8_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree83" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree83" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian8_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree93">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree93" aria-expanded="false"
                                aria-controls="flush-collapseThree93">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian9_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree93" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree93" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian9_data_html}
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree1010">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree1010" aria-expanded="false"
                                aria-controls="flush-collapseThree1010">
                                <span class="ps-2">${contentModel.fAQsTermsofAppointmentaccordian10_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree1010" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree1010" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.fAQsTermsofAppointmentaccordian10_data_html}
                            </div>
                        </div>
                    </div>
                </div>
                <h3 <@studio.iceAttr />>${contentModel.faqsSecurityClearanceheader_s}</h3>
                <div class="accordion accordion-flush" id="accordionFlushExample4" <@studio.iceAttr />>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree103">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree103" aria-expanded="false"
                                aria-controls="flush-collapseThree103">
                                <span class="ps-2">${contentModel.faqsSecurityClearanceaccordian1_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree103" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree103" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.faqsSecurityClearanceaccordian1_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree113">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree113" aria-expanded="false"
                                aria-controls="flush-collapseThree113">
                                <span class="ps-2">${contentModel.faqsSecurityClearanceaccordian1_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree113" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree113" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.faqsSecurityClearanceaccordian1_data_html}
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree123">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree123" aria-expanded="false"
                                aria-controls="flush-collapseThree123">
                                <span class="ps-2">${contentModel.faqsSecurityClearanceaccordian1_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree123" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree123" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.faqsSecurityClearanceaccordian1_data_html}
                            </div>
                        </div>
                    </div>
                </div>

                <h3 <@studio.iceAttr />>${contentModel.taxationheader_s}</h3>
                <div class="accordion accordion-flush" id="accordionFlushExample5" <@studio.iceAttr />>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree113">
                            <button class="accordion-button collapsed ps-5" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree113" aria-expanded="false"
                                aria-controls="flush-collapseThree113">
                                <span class="ps-2">${contentModel.taxationaccordian1_html}</span>
                            </button>
                        </h2>
                        <div id="flush-collapseThree113" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree113" data-bs-parent="#accordionFlushExample3">
                            <div class="accordion-body">
                                ${contentModel.taxationaccordian1_data_html}
                            </div>
                        </div>
                    </div>
                </div>
                <h3 <@studio.iceAttr />>${contentModel.privacyStatementHeader_s}</h3>
                <p <@studio.iceAttr />>${contentModel.privacyStatementcontent_html}</p>
            </div>
        </div>
    </section>
    <@renderComponent component=contentModel.footer_o.item />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <@studio.toolSupport />

</body>

</html>