<#import "/templates/system/common/cstudio-support.ftl" as studio />
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Bootstrap Static Navbar</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>


    <style>
        * {
            box-sizing: border-box;
        }
        
        .about h1{
            color: #80def5;
        }
        
        .about {
            margin: 50px 0;
        }
        
       .about_data h2{
            margin: 15px 0 30px 0;
            font-size: 1.8em;
            font-family: 'Roboto', sans-serif;
        }
        
        .show{
            display:block;
        }
        .hide{
            display:none;
        }
    </style>
</head>

<body>
<@renderComponent component=contentModel.header_o.item />
    <div id="carouselExampleControls" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="${contentModel.carouselimag1_s}" class="d-block w-100 h-75" alt="..." <@studio.iceAttr /> />
            </div>
            <div class="carousel-item">
                <img src="${contentModel.carouselimag2_s}" class="d-block w-100 h-75" alt="..." <@studio.iceAttr /> />
            </div>
            <div class="carousel-item">
                <img src="${contentModel.carouselimag3_s}" class="d-block w-100 h-75" alt="..." <@studio.iceAttr /> />
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next float-end" type="button" data-bs-target="#carouselExampleControls"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <section class="about">
        <div class="container">
            <div class="bg-dark text-info px-4 py-2">
                <h1 <@studio.iceAttr />>${contentModel.aboutheader_s}</h1>
            </div>
            <div class="about_data">
                <h2 <@studio.iceAttr />>${contentModel.aboutsubheading_s}</h2>
                <p <@studio.iceAttr />>${contentModel.aboutcontent_html}</p>
                <h2 <@studio.iceAttr />><u>${contentModel.covidheader_s}</u></h2>
                <p <@studio.iceAttr />>${contentModel.covidcontent_html}</p>
            </div>
            <div class="text-center mt-2">
               
                <p <@studio.iceAttr />>${contentModel.appicationdetails_html}</p>
                 <p  id="errorMsg" class="hide"> </p>
                <form  id="notify-form" method="post" action="#" runat="server">
                <label for="firstName" <@studio.iceAttr />>${contentModel.firstNameLabel_s}</label>
                <input type="text" class="form-control" id="firstName" name="firstName" required ></input>
                <label for="lastName" <@studio.iceAttr />>${contentModel.lastNameLabel_s}</label>
                <input type="text" class="form-control" id="lastName" name="lastName" required ></input>
                <label for="email" <@studio.iceAttr />>${contentModel.emaillabel_s}</label>
                <input type="email" class="form-control" id="email" name="email" required ></input>
                <input type="button" class="btn btn-outline-secondary mt-2" onclick=" requestForProgramStart()" <@studio.iceAttr /> value="${contentModel.notificationbutton_s}"></input>
                </form>
            </div>
           
        </div>
    </section>
<@renderComponent component=contentModel.footer_o.item />
    <@studio.toolSupport />
    <script>
function requestForProgramStart(){
            var firstNameData= document.getElementById("firstName").value;
            var lastNameData= document.getElementById("lastName").value;
            var emailToBeSent= document.getElementById("email").value;
       
          var dataToSend = {
                    firstName: firstNameData,
                    lastName: lastNameData,
                    email: emailToBeSent,
                    eventUserJson: null
                };
                 
               const response =  fetch('/api/EventUser.json', {
                      method: "POST",
                      body: JSON.stringify(dataToSend),
                      headers: {"Content-type": "application/json; charset=UTF-8","X-ProgramID": ${contentModel.programIDToCallApi_s}}
                    })
                    .then(response =>{
                       if(response.status==200){
                           var element = document.getElementById("errorMsg");
                            element.classList.remove("hide");
                            document.getElementById("notify-form").reset();
                            element.innerHTML = "you will be notified once the application portal is open!";
                            element.classList.add("show");
                       }
                       else{
                        var element = document.getElementById("errorMsg");
                        element.classList.remove("hide");
                        document.getElementById("notify-form").reset();
                        element.innerHTML = "There was a problem saving your notification request";
                        element.classList.add("show");
                       }
                      
                    return response.json()});
                    document.getElementById("notify-form").reset();
    }
	</script>
</body>

</html>