<#import "/templates/system/common/cstudio-support.ftl" as studio />
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <style>
        body {
            font-family: "Open Sans", sans-serif;
        }

        * {
            box-sizing: border-box;
        }

        .labContacts h1{
            color: #80def5;
        }
        
        .labContacts {
            margin: 50px 0;
        }
        
        .labContacts_data h2 {
            margin: 15px 0 30px 0;
            font-size: 1.8em;
            font-family: 'Roboto', sans-serif;
        }
        
        .labContacts_data p {
            margin-bottom: 30px;
            line-height: 2em;
        }
        
        .labContacts_data {
            text-transform: capitalize;
        }
        .labContacts_data a{
            color:#0d4edb;
            text-decoration: none;
        }

        
        
    </style>
</head>

<body>
<@renderComponent component=contentModel.header_o.item />
    <section class="labContacts">
        <div class="container">
            <div class="bg-dark px-4 py-2">
                <h1 <@studio.iceAttr />>${contentModel.labcontactsheader_s}</h1>
            </div>
            <div class="labContacts_data">
              <div <@studio.iceAttr />>
                <p>${contentModel.labcontactsquestion1_html}</p>
                <p>${contentModel.labcontactsquestion2_html}</p>
                <p>${contentModel.labcontactsquestion3_html}</p>
                
                <p>${contentModel.labcontactsquestion4_html}</p>
                
                <p>${contentModel.labcontactsquestion5_html}</p>
                
                <p>${contentModel.labcontactsquestion6_html}</p>
                
                <p>${contentModel.labcontactsquestion7_html}</p>
                
                <p>${contentModel.labcontactsquestion8_html}</p>
                
                <p>${contentModel.labcontactsquestion9_html}</p>
                
                <p>${contentModel.labcontactsquestion10_html}</p>
                
                <p>${contentModel.labcontactsquestion11_html}</p>
                
                <p>${contentModel.labcontactsquestion12_html}</p>
                
                <p>${contentModel.labcontactsquestion13_html}</p>
                
                <p>${contentModel.labcontactsquestion14_html}</p>
                
                <p>${contentModel.labcontactsquestion15_html}</p>
                
                <p>${contentModel.labcontactsquestion16_html}</p>
                
                <p>${contentModel.labcontactsquestion17_html}</p>
                
                <p>${contentModel.labcontactsquestion18_html}</p>
                
                <p>${contentModel.labcontactsquestion19_html}</p>
                
                <p>${contentModel.labcontactsquestion20_html}</p>
                
                <p>${contentModel.labcontactsquestion21_html}</p>
                
                <p>${contentModel.labcontactsquestion22_html}</p>
                
                <p>${contentModel.labcontactsquestion23_html}</p>
                
                <p>${contentModel.labcontactsquestion24_html}</p>
                
                <p>${contentModel.labcontactsquestion25_html}</p>
                
                <p>${contentModel.labcontactsquestion26_html}</p>
                
                <p>${contentModel.labcontactsquestion27_html}</p>
                
                <p>${contentModel.labcontactsquestion28_html}</p>
                
                <p>${contentModel.labcontactsquestion29_html}</p>
                
                <p>${contentModel.labcontactsquestion30_html}</p>
                
                <p>${contentModel.labcontactsquestion31_html}</p>
                
                <p>${contentModel.labcontactsquestion32_html}</p>
                
                <p>${contentModel.labcontactsquestion33_html}</p>
                
                <p>${contentModel.labcontactsquestion34_html}</p>
                
                <p>${contentModel.labcontactsquestion35_html}</p>
                
                <p>${contentModel.labcontactsquestion36_html}</p>
                
                <p>${contentModel.labcontactsquestion37_html}</p>
                
                <p>${contentModel.labcontactsquestion38_html}</p>
                
              </div>
            </div>
        </div>
    </section>
<@renderComponent component=contentModel.footer_o.item />
    <@studio.toolSupport />

</body>

</html>