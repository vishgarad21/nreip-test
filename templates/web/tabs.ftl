<html>
<head>
<style>
.nav-item  {
    display: inline-block;
    margin:0 10 px;
}
div{
    background-color:#000000
}
</style>
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About-Us</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
   

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
        integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
        crossorigin="anonymous"></script>
</head>
<body>
<div >
		<nav class="navbar navbar-dark navbar-expand-md navbar-expand-sm  ">
			
					<ul class="navbar-nav-tabs">
						<li class="nav-item"> <a class="nav-link" href="#"> ABOUT NREIP</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#"> DETAILS</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#">  LAB CONTACTS</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#"> LABS</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#"> ELIGIBILITY</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#"> FAQ</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#"> EVENTS</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#"> PROMOTIONAL POSTER</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#"> INTERN QUAD CHARTS</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#"> SUMMER 2021 COVID LAB STATUS</a> </li>
						<li class="nav-item"> <a class="nav-link" href="#"> APPLY</a> </li>
					</ul>
				</div>
			</div>
		</nav>
		
	</div>
	</body>
	</html>